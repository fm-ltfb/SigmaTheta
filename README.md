# SigmaTheta software set

Copyright or © or Copr. Université de Franche-Comté, Besançon, France
Contributor: François Vernotte, UTINAM/OSU THETA (2012/07/17)
Contact: francois.vernotte@obs-besancon.fr

This software, SigmaTheta, is a collection of computer programs for
time and frequency metrology.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


## Installation

Please consult the INSTALL file in this distribution for more
detailed instructions.

## Available executables

* *1col2col*

Transforms a 1 column file into a 2 column file.

* *3CHGraph*

Plots 3-cornered hat results (computed by Groslambert Covariance or classical 3-cornered hat) with confidence intervals in a single graph.

* *3CorneredHat*

Computes the Allan deviation of each clock of a 3-cornered hat system, estimates the confidence intervals and plots the results.

* *ADev*

Computes the Allan Deviations of a sequence of normalized frequency deviation measurements.

* *ADGraph*

Plots the graph of the (modified) Allan Deviation estimates versus tau.

* *ADUncert*

Computes the 95 % (2 sigma) and the 68% (1 sigma) confidence intervals of a sequence of (modified) Allan Deviations.

* *Asym2Alpha*

Finds the dominating power law noise (alpha) versus tau.

* *Asymptote*

Computes the tau^-3/2, tau^-1, tau^-1/2, tau^0, tau^1/2 and tau asymptotes of a sequence of (modified) Allan Deviations.

* *AVarDOF*

Compute the degrees of freedom of the variance computations over a sequence of tau values.

* *Aver*

Computes the average of the 2nd columns of FILE1, FILE2 and FILE3: (y1+y2+y3)/3.

* *bruiteur*

Computes a sequence of simulated time error x(t) and/or normalized frequency deviation yk samples.

* *DevGraph*

Plots the graph of 1 to 8 2-columns Dev files (suitable to plot the 3 ADEV computed by GCoDev on a three-cornered hat system and the ADEV of the measurement noise computed by Aver).

* *DriRem*

Removes the linear drift of a sequence of normalized frequency deviation measurements.

* *GCoDev*

Computes the Groslambert CoDeviations (see arXiv:1904.05849) of 2 sequences of normalized frequency deviation measurements.

* *GCUncert*

Computes the 95 % confidence interval from GCov measurements.

* *HDev*

Computes the modified Hadamard Deviations of a sequence of normalized frequency deviation measurements.

* *MDev*

Computes the modified Allan Deviations of a sequence of normalized frequency deviation measurements.

* *PDev*

Computes the Parabolic Deviations of a sequence of normalized frequency deviation measurements.

* *PSDGraph*

Plots the graph of the Power Spectrum Density (PSD) of normalized frequency deviation versus the frequency.

* *RaylConfInt*

Computes the mean and the 95 % confidence interval of a chi distribution with 'value' degrees of freedom, normalized by the square root of 'value'.

* *SigmaTheta*

Computes the (modified) Allan Deviations of a sequence of normalized frequency deviation measurements, the 95 % confidence intervals (2 sigma), the 68 % confidence intervals (1 sigma), the asymptotes and plots a graph.

* *uncertainties*

Computes the 95 % confidence intervals of a sequence of (modified) Allan Deviations, the asymptotes and plot a graph.

* *X2Y*

Transforms a time error sequence {x(t)} into a normalized frequency deviation sequence {Yk}.

* *XtGraph*

Plots the graph of the time error data versus time.

* *YkGraph*

Plots the graph of the normalized frequency deviation samples versus time.

## Sample scripts

1. *STshell*  

STshell is an example of bourne shell chaining ADev and uncertainties for obtaining the same result as SigmaTheta.

Same input and output format as SigmaTheta.

2. *STshell2*  

STshell2 is an example of bourne shell chaining ADev, Asymptote, Asym2Alpha, AVarDOF, ADUncert and ADGraph for obtaining the same result as SigmaTheta when the classical Allan variance is selected from the configuration file.

Same input and output format as SigmaTheta.

3. *STshell3*  

STshell3 is an example of bourne shell chaining MDev, Asymptote, Asym2Alpha, AVarDOF, ADUncert and ADGraph for obtaining the same result as SigmaTheta when the modified Allan variance is selected from the configuration file.

Same input and output format as SigmaTheta.

4. *GCDshell*  

GCDshell is an example of bourne shell chaining GCoDev, Aver, ADev, ..., GCUncert and 3CHGraph to obtain more or less the same result as with 3CorneredHat.

## Configuration
The default configuration file is available in the source distribution as SigmaTheta.conf.
It is copied upon installation to a systemwide location in /usr/share/sigmatheta/SigmaTheta.conf.
This copy will be used unless a customized alternative is present in the user's home
directory under the name .SigmaTheta.conf.

Several options
can be set or unset by typing ON or OFF after its description. The
default configuration is ON for all options exept the Tau^-3/2 slope fit
and the modified Allan variance:

Option : Default

68% bounds: ON  
95% bounds: ON  
Unbiased estimates: ON  
Graph: ON  
Title: ON  
Fit: ON  
Double Fit: ON  
Asymptotes: ON  
Tau^-3/2 slope: OFF  
Tau^-1 slope: ON  
Tau^-1/2 slope: ON  
Tau^0 slope: ON  
Tau^1/2 slope: ON  
Tau^1 slope: ON  
Modified Allan variance: OFF  
Tau: 1 2 3 5  
Output: PDF  

=====================================================================  
François Vernotte, 2012/07/17 (initial), 2023/05/03 (latest revision)

